library(openxlsx)
source("R/hitlogic.R")
source("R/getpvalcutoff.R")

pathwayERaccuracy = function(method = "fc", dataset = "user_wneg", pathset = "bhrr", nametag = NULL, 
                             potency = "ac50", pvals = c(.1,.05,.01,.005,.001)){
  
  erscores = read.xlsx("S2 ER SuperMatrix 2015-03-24.xlsx")
  
  keepcols = c("CODE", "CASRN", "Name", "AUC.Agonist", "AUC.Antagonist")
  if(potency == "ac50") erscores = erscores[,c(keepcols,"pseudo.AC50.median")]
  
  if(!is.null(nametag)) nametag = paste0("_", nametag)
  
  bmad_factor = 0.2
  file <- paste0("output/pathway_conc_resp_summary/PATHWAY_CR_",pathset,"_",dataset,"_",method ,"_",bmad_factor,
                 nametag,".RData")
  load(file)
  
  keepcols = c("sample_id", "dsstox_substance_id", "casrn", "name", "pathway", "cutoff", "fit_method", "bmd10", "bmdl",
               "bmdu", "ac50", "top", "conc", "resp")
  PATHWAY_CR = PATHWAY_CR[,keepcols]
  
  allpaths = unique(PATHWAY_CR$pathway)
  # keeppaths = allpaths[grepl("estrogen|estradiol",allpaths, ignore.case = T)]
  keeppaths = allpaths
  PATHWAY_CR = PATHWAY_CR[PATHWAY_CR$pathway %in% keeppaths,]
  
  statnames = c("MAE", "TPR", "TNR", "BA", "RMSE")
  statdf = data.frame(matrix(nrow = length(keeppaths)*length(pvals), ncol = length(statnames)+2))
  colnames(statdf) = c("pathway", "pvalue", statnames)
  statdf$pathway = rep(keeppaths, length(pvals))
  statdf$pvalue = rep(pvals, each = length(keeppaths))
  
  ers = erscores$CASRN[erscores$AUC.Agonist >= .1 | erscores$AUC.Antagonist >= .1]
  
  if(potency == "ac50"){
    PATHWAY_CR$truepot = -log10(erscores$pseudo.AC50.median[match(PATHWAY_CR$casrn ,erscores$CASRN)])
    PATHWAY_CR$predpot = -log10(PATHWAY_CR$ac50)
  }
  
  pvalkeymaster = getpvalcutoff(pathset, nullset = paste0(dataset, "_RAND125"), method = method, pvals = pvals)
  
  for(pval in pvals){
    pvalkey = pvalkeymaster[pvalkeymaster$pvalue == pval,]
    newcutoff = pvalkey$cutoff[match(PATHWAY_CR$pathway, pvalkey$pathway)]
    PATHWAY_CR$hitcall = hitlogic(PATHWAY_CR, newcutoff = newcutoff)

    #P/N by pathway
    for(pway in keeppaths){
      newpcr = PATHWAY_CR[PATHWAY_CR$pathway == pway,]
      P = sum(newpcr$casrn %in% ers)
      TP =  sum(newpcr$casrn %in% ers & newpcr$hitcall == 1)
      N = sum(!newpcr$casrn %in% ers)
      TN = sum((newpcr$hitcall == 0) & (!newpcr$casrn %in% ers))
      RMSE = sqrt(mean( ((newpcr$predpot - newpcr$truepot)[newpcr$casrn %in% ers & newpcr$hitcall == 1])^2))
      MAE = mean( abs((newpcr$predpot - newpcr$truepot)[newpcr$casrn %in% ers & newpcr$hitcall == 1]))
      
      statdf[statdf$pathway == pway & statdf$pvalue == pval, "RMSE"] = RMSE
      statdf[statdf$pathway == pway & statdf$pvalue == pval, "TPR"] = TP/P
      statdf[statdf$pathway == pway & statdf$pvalue == pval, "TNR"] = TN/N
      statdf[statdf$pathway == pway & statdf$pvalue == pval, "BA"] = (TP/P + TN/N)/2
      statdf[statdf$pathway == pway & statdf$pvalue == pval, "MAE"] = MAE

    }

  }
  
  statdf = statdf[order(-statdf$BA, statdf$RMSE),]
  
  write.xlsx(statdf, paste0("output/pathwayERaccuracy/stats_",pathset,"_",dataset,"_",method,"_",potency,nametag,"_allpaths.xlsx"))
  
}