library(ToSCR)
library(parallel)
options(mc.cores = detectCores() - 4)
options(stringsAsFactors = FALSE)
# library(WriteXLS)
# source("httrws.R")

## =================================================================
## Functions
getdata <- function(epaid) {
    Y <- getHTTrCountData(epaid,probe_filter='av0_5')
    dtameta <- Y$treatment
    dtameta$ID <- paste(dtameta$plate_id, dtameta$well_id, sep="_")
    Ksub <- Y$counts
    rownames(Ksub) <- dtameta$ID
    ## We still need the readcounts for these samples
    ## First get the plates
    plates <- unique(dtameta$plate_id)
    rc <- unlist(lapply(plates, function(plateid) {
        x <- getHTTrPlateInfo(plateid)
        structure(as.numeric(x$n_reads),
                  names=paste(x$plate_id, x$well_id, sep="_"))
    }))
    dtameta$rc <- rc[dtameta$ID]
    dtameta$conc <- as.numeric(dtameta$conc)
    dtameta$timeh <- as.numeric(dtameta$timeh)
    list(treatment=dtameta, counts=Ksub)
}

## =================================================================
nms <- searchHTTrChem('Coumarin')

X <- getdata(nms[1,3])

Fits <- toscrfit(conc = X$treatment$conc, K = X$counts,
                 Norm = X$treatment$rc / 3e6, plateid = X$treatment$plate_id,
                 R = 1.1, BMD_CI = 0.90, ncores=getOption("mc.cores"))
save(Fits, file="CoumarinFits.RData")
load("CoumarinFits.RData")
zz <- summary(Fits)

WriteXLS("zz", "CoumarinFits.xlsx",AdjWidth=TRUE, BoldHeaderRow=TRUE, FreezeRow=1)

## Build plots
## plot fits for probes with non-constant curves and abs(Top) > 1
Probes <- zz$probe[!is.na(zz$Top) & zz$bestModel != "Const" & abs(zz$Top) > 1]

pdf("NCFits.pdf", width=7, height=7)
plot(Fits, type="CR", probes=Probes)

dev.off()

