library(httRws)
library(parallel)
library(pbmcapply)

FCMATDownload = function(){
  timestart  = proc.time()
  
  trycount = 0
  while(trycount < 50){
    chemdict = try(searchHTTrChem(""), silent = T)
    if (class(chemdict)== "try-error"){
      trycount = trycount + 1
      cat("Failed Attempts: ", trycount, "\n")
    } else {
      break
    }
  }
  if(trycount == 50) stop("Timeout while retrieving chemical list")
  dtxs = chemdict$dsstox_sid
  dtxs = unique(as.character(levels(dtxs))[dtxs])
  print(length(dtxs))
  
  listout = pbmclapply(as.list(dtxs),function(x){
    rawout = NULL
    trycount = 0
    while(trycount < 50){
      rawout = try(getHTTrDEG(x, pivot_on = "gene", pivot_func = "max")[[1]], silent = T)
      if (class(rawout)== "try-error" | is.null(rawout)){
        trycount = trycount + 1
      } else {
        break
      }
    }
    cat("Failed Attempts: ", trycount, "\n")
    if(trycount == 50) return(rawout)
    
    charcols = c("chem_name", "epa_sample_id", "dsstox_sid", "trt_grp_id")
    numcols = colnames(rawout)[!colnames(rawout) %in% charcols]
    # numout = as.data.frame(lapply(rawout[,numcols], function(x){as.numeric(levels(x))[x]}), stringsAsFactors = F)
    numout = rawout[,numcols]
    charout = as.data.frame(lapply(rawout[,charcols], function(x){as.character(levels(x))[x]}), stringsAsFactors = F)
    return(cbind(charout,numout))
  }, mc.cores = 35)
  
  listclass = Reduce(c,lapply(listout,class))
  empties = sapply(listclass, function(x){x != "data.frame"})
  cat("Retrieval failed for ", sum(empties), " chemicals\n")
  if(sum(empties) < 100) print(dtxs[empties])
  
  cat(proc.time() - timestart)
  return(listout)
  
}

listmerge = function(dflist){
  timestart  = proc.time()
  
  listclass = Reduce(c,lapply(dflist,class))
  empties = sapply(listclass, function(x){x != "data.frame"})
  cat("Ignoring ", sum(empties), " failed retrievals\n")
  dflist = dflist[!empties]
  
  finalrows = sum(sapply(dflist,nrow))
  finalcolnames = unique(Reduce(c,lapply(dflist,colnames)))
  
  outframe = data.frame(matrix(NA, nrow = finalrows, ncol = length(finalcolnames)))
  colnames(outframe) = finalcolnames
  outframe$trt_grp_id = Reduce(c,sapply(dflist,function(x){x$trt_grp_id}))
  
  for(x in dflist) {
   outframe[match(x$trt_grp_id, outframe$trt_grp_id),match(colnames(x), colnames(outframe))] = x
   
  }
  cat(proc.time() - timestart)
  return(outframe)
  
}

FCMATwrite = function(dfmerge){
  dictcols = c("trt_grp_id", "epa_sample_id", "conc", "timeh", "chem_name", "dsstox_sid")
  CHEM_DICT = dfmerge[,dictcols]
  CHEM_DICT = unique(CHEM_DICT)
  colnames(CHEM_DICT) = c("sample_key", "sample_id", "conc", "time", "name", "dsstox_substance_id")
  rownames(CHEM_DICT) = CHEM_DICT$sample_key
  
  dtxtocasrn = read.csv("dtxtocasrn.csv", stringsAsFactors = F)
  CHEM_DICT$casrn = dtxtocasrn$CASRN[match(CHEM_DICT$dsstox_substance_id, dtxtocasrn$DTXSID)]
  nameorder = c("sample_key", "sample_id", "conc", "time", "casrn","name", "dsstox_substance_id")
  CHEM_DICT = CHEM_DICT[,match( nameorder, colnames(CHEM_DICT))]
  
  save(CHEM_DICT, file = "input/fcdata/CHEM_DICT_Phase1_6.RData")
  
  FCMAT2 = dfmerge
  rownames(FCMAT2) = dfmerge$trt_grp_id
  FCMAT2 = as.matrix(FCMAT2[,-which(colnames(FCMAT2) %in% dictcols)])
  
  save(FCMAT2, file = "input/fcdata/FCMAT2_Phase1_6.RData")
  
}
