source("R/general_utils.R")
library(openxlsx)
#--------------------------------------------------------------------------------------
#' Compare the data from duplicates at the gene level
#' @param dir The directory where the analyses will be put
#' @param method Either gene or probe_id
#' @param to.file If TRUE, plot the data to a pdf file
#'
#' @return Nothing is returned
#' Two files are generated, a pdf of the plots and an Excel of the fit parameters
#'
#--------------------------------------------------------------------------------------
pathwayDuplicateCompare <- function(dir="output/screen_duplicate_comparev2/",
                                   bmad_factor=3,
                                   pathset = "ALL",
                                   dataset = "Phase1_6reps",
                                   method = "fc",
                                   to.file=T,
                                   metric = "top_over_bmad",
                                   nullset = "Phase1_6fixed_RAND10000",
                                   ACs = c(.5, 10)) {
  printCurrentFunction()
  pathdatamethod = paste0(pathset, "_", dataset, "_", method)
  if(to.file) {
    fname <- paste0(dir,"duplicate_compare_",bmad_factor, "_", pathdatamethod,"_",metric, "_1.pdf")
    pdf(file=fname,width=8,height=10,pointsize=12,bg="white",paper="letter",pagecentre=T)
    par(mfrow=c(3,2),mar=c(4,4,2,2))
  }

  
  file = paste0("output/pathway_conc_resp_summary/PATHWAY_CR_", pathdatamethod, "_1.RData")
  load(file)
  
  PATHWAY_CR = PATHWAY_CR[PATHWAY_CR$name != "Genistein",]  #remove Genistein
  if(metric == "top_over_bmad") PATHWAY_CR$top_over_bmad = PATHWAY_CR$top/PATHWAY_CR$cutoff*PATHWAY_CR$bmad_factor
  

  if(metric == "top_over_nullbmad"){
    file <- paste0("output/pathway_score_summary/PATHSCOREMAT_",pathset,"_",nullset,"_",method,".RData")
    load(file) # gets nullset
    #use pvalue bmad
    nullvals = aggregate(pathway_score ~ pathway, pathscoremat, mad)
    PATHWAY_CR$bmad = nullvals$pathway_score[match(PATHWAY_CR$pathway,nullvals$pathway)]
    PATHWAY_CR$top_over_nullbmad = PATHWAY_CR$top/PATHWAY_CR$bmad
  }
  
  if(metric == "abslogpvalue"){
    file <- paste0("output/pathway_score_summary/PATHSCOREMAT_",pathset,"_",nullset,"_",method,".RData")
    load(file) # gets nullset
    #use -log10(pvalue) instead of pvalue so that bigger is stricter
    #add small number 1e-5 so that 0 gets assigned a number
    nullvals = aggregate(pathway_score ~ pathway, pathscoremat, list)
    null.list = nullvals$pathway_score
    names(null.list) = nullvals$pathway
    PATHWAY_CR$abslogpvalue = mapply(function(pway, top){abs(log10(1e-5 + sum(abs(null.list[[pway]] ) >= abs(top) )/length(null.list[[pway]])))},
                               pway = PATHWAY_CR$pathway,top = PATHWAY_CR$top)
  }
  
  cnames = unique(PATHWAY_CR$name)
  
  
  # file <- paste0("output/pathway_score_summary/PATHSCOREMAT_",pathset,"_",dataset,"_fc.RData")
  # load(file)
  # 
  # meanmeans = aggregate(mean_fc_scaled_in ~ pathway + sample_id + dsstox_substance_id, pathscoremat, function(x){
  #   pos = which.max(abs(x))
  #   return(x[pos])
  # })
  # PATHWAY_CR = merge(meanmeans, PATHWAY_CR, all.x = F, all.y = T)
  # colnames(PATHWAY_CR)[colnames(PATHWAY_CR) == "mean_fc_scaled_in"] = "top_over_bmad"
  
  #create custom bin.list based on quantiles of whole data abs(top_over_bmad)
  # quants = quantile(abs(PATHWAY_CR$top_over_bmad[!is.na(PATHWAY_CR$ac50)]), (1:10/10))
  # quants = unique(round(quants,2))
  # bin.list = sort(c(-quants, 0, quants))
  # bin.list = quantile((PATHWAY_CR$top_over_bmad[!is.na(PATHWAY_CR$ac50)]), (0:19/20 + .025))
  step = quantile((PATHWAY_CR[,metric][!is.na(PATHWAY_CR$ac50)]), .999)
  step= signif(step/9,1)
  bin.list = -9:9*step
  
  hitsdf = vector(length(cnames),mode = "list")
  names(hitsdf) = cnames
  # TOB_bothhit = vector(length(cnames),mode = "list") #keep both TOB values
  # TOB_onehit = vector(length(cnames),mode = "list")
  # TOB_bothhit_min = vector(length(cnames),mode = "list") #
  # TOB_bothhit_max = vector(length(cnames),mode = "list")
  # DAC50_bothhit = vector(length(cnames),mode = "list")
  # names(TOB_bothhit) = names(TOB_onehit) = names(TOB_bothhit_min) = names(TOB_bothhit_max) = names(DAC50_bothhit) = cnames
  # 
  for(cname in cnames) {
    if(is.numeric(bmad_factor)) cat(cname,"\n")
    data = PATHWAY_CR[PATHWAY_CR$name == cname,]
    samples = unique(data$sample_id)
    if(length(samples) == 1) {
      print("Chemical not replicated")
      break
    }
    if(length(samples) > 2) {
      print("Three or more replicates: using first two")
      samples = samples[1:2]
    }
    
    data1 = data[data$sample_id == samples[1],]
    data2 = data[data$sample_id == samples[2],]
    rm(data)
    data1 = data1[!duplicated(data1$pathway),]
    data2 = data2[!duplicated(data2$pathway),]    
    rownames(data1) <- data1[,"pathway"]
    rownames(data2) <- data2[,"pathway"]
    n1 <- nrow(data1)
    n2 <- nrow(data2)
    path.list <- data1[,"pathway"]
    path.list <- path.list[is.element(path.list,data2[,"pathway"])]
    path.list <- sort(path.list)
    n12 <- length(path.list)
    data1 <- data1[path.list,]
    data2 <- data2[path.list,]
    
    #plot(c(1,1),main=cname,xlim=c(1e-3,100),ylim=c(-5,5),log="x",xlab="log(conc)",ylab="log2FC",type="n")
    #lines(c(1e-6,0),c(1e6,0))
    x1 <- log10(data1[,"ac50"])
    y1 <- data1[,metric]
    y1[is.na(x1)] <- NA
    h1 <- data1[,"hitcall"]
    x2 <- log10(data2[,"ac50"])
    y2 <- data2[,metric]
    y2[is.na(x2)] <- NA
    h2 <- data2[,"hitcall"]
    delta.pot <- x1
    delta.pot[] <- NA
    extreme <- delta.pot
    replicate.data <- delta.pot
    replicate.hit <- delta.pot
    replicate.gt.cutoff <- delta.pot
    replicate.data[] <- 0
    replicate.hit[] <- 0
    replicate.gt.cutoff[] <- 0
    if(is.numeric(bmad_factor)){
      for(i in 1:length(path.list)) {
      if(!is.na(x1[i]) && !is.na(y1[i]) && !is.na(x2[i]) &!is.na(y2[i])) replicate.data[i] <- 1
      if(h1[i]==1 && h2[i]==1) replicate.hit[i] <- 1
      if(replicate.data[i]==1) {
        maxval <- max(abs(y1[i]),abs(y2[i]))
        if(maxval==abs(y1[i])) extreme[i] <- y1[i]
        else if(maxval==abs(y2[i])) extreme[i] <- y2[i]
        delta.pot[i] <- abs(x1[i]-x2[i])
      }
      if(!is.na(y1[i]) && !is.na(y2[i]) ) {
        if(abs(y1[i])>bmad_factor && abs(y2[i])>bmad_factor) replicate.gt.cutoff[i] <- 1
        else if(abs(y1[i])>bmad_factor || abs(y2[i])>bmad_factor) replicate.gt.cutoff[i] <- -3
      }
      else if(!is.na(y1[i])) {
        if(abs(y1[i])>bmad_factor) replicate.gt.cutoff[i] <- -1
      }
      else if(!is.na(y2[i])) {
        if(abs(y2[i])>bmad_factor) replicate.gt.cutoff[i] <- -2
      }
    }
    }
      
    mat <- cbind(x1,x2,y1,y2,h1,h2,replicate.gt.cutoff)
    if(is.numeric(bmad_factor)){
      if(length(delta.pot)>2) {
      bins <- extreme
      bins[] <- 0
      # bin.list <- c(-10,-6,-4,-3.5,-3,-2.5,-2,-1.5,-1,0,1,1.5,2,2.5,3,3.5,4,6,10)
      for(i in 1:length(path.list)) {
        x <- extreme[i]
        if(!is.na(x)) bins[i] <- bin.list[which.min(abs(bin.list-x))]
      }
      nboth.gt.cutoff <- length(replicate.gt.cutoff[replicate.gt.cutoff==1])
      n1.gt.cutoff <- length(replicate.gt.cutoff[replicate.gt.cutoff==-1])
      n2.gt.cutoff <- length(replicate.gt.cutoff[replicate.gt.cutoff==-2])
      n3.gt.cutoff <- length(replicate.gt.cutoff[replicate.gt.cutoff==-3])
      count.list <- bin.list
      count.list[] <- 0
      for(i in 1:length(bin.list)) {
        bin <- bin.list[i]
        count.list[i] <- sum((bins==bin) &  !is.na(delta.pot)) #now disregards NAs
      }
      if(sum(!is.na(delta.pot)) > 0){
        boxplot(delta.pot~bins,ylim=c(0,6),main=paste(cname,"\n",n1,n2,n12),
                xlab=paste0("Extreme of Pathway Score ", metric),ylab="|delta(log10(AC50))|")
      } else {
        plot(c(1,1),type="n",ylim=c(0,6),main=paste(cname,"\n",n1,n2,n12),
             xlab=paste0("Extreme of Pathway Score ", metric),ylab="|delta(log10(AC50))|")
      }

      x <- 0
      for(i in 1:length(bin.list)) {
        if(count.list[i]>0) {
          x <- x+1
          text(x,6,count.list[i])
        }
      }
      x1a <- x1[replicate.gt.cutoff==1]
      x2a <- x2[replicate.gt.cutoff==1]
      x1b <- x1[replicate.gt.cutoff== -3]
      x2b <- x2[replicate.gt.cutoff== -3]
      h1a <- h1[replicate.gt.cutoff==1]
      h2a <- h2[replicate.gt.cutoff==1]
      h1b <- h1[replicate.gt.cutoff== -3]
      h2b <- h2[replicate.gt.cutoff== -3]
      mask <- h1b*h2b
      x1b <- x1b[mask==1]
      x2b <- x2b[mask==1]

      mask <- h1*h2
      x1m <- x1[mask==1]
      x2m <- x2[mask==1]
      if(length(x1m)>0 || length(x1m)>0) {
        plot(x2m~x1m,type="p",pch=".",xlab= expression(Duplicate~A~log[10](AC[50])),
             ylab=expression(Duplicate~B~log[10](AC[50])),xlim=c(-3,3),
             ylim=c(-4,3),
             main=paste(cname,"\n",nboth.gt.cutoff,n3.gt.cutoff,n1.gt.cutoff,n2.gt.cutoff))
        lines(c(-6,6),c(-6,6))
        points(x2b~x1b,pch=21,bg="blue")
        points(x2a~x1a,pch=21,bg="red")
        legend("bottomleft", c("Both Hit Below Cutoff","One Above and One Below Cutoff","Both Hits Above Cutoff"),
               pch = c(46, 21, 21), pt.bg = c("black", "blue", "red"))
      }
      else {
        plot(c(1,1),type="n",xlab=expression(Duplicate~A~log[10](AC[50])),ylab=expression(Duplicate~B~log[10](AC[50])),xlim=c(-3,3),ylim=c(-3,3),main=paste(cname,"\n",nboth.gt.cutoff,n3.gt.cutoff,n1.gt.cutoff,n2.gt.cutoff))
        lines(c(-6,6),c(-6,6))
      }
      breaks <- c(seq(from=-4,to=4,by=0.25))
      x <- x1[h1==1]
      hist(x,breaks=breaks,main=paste(cname,"\nDuplicate A"),xlab=expression(log[10](AC[50])))
      x <- x2[h2==1]
      hist(x,breaks=breaks,main=paste(cname,"\nDuplicate B"),xlab=expression(log[10](AC[50])))

      x <- x1[h1==1]
      y <- y1[h1==1]
      ylimit = ceiling(max(c(abs(y1[!is.na(y1)]), abs(y2[!is.na(y2)]), bmad_factor)))
      y1bad = y1[h1 == 1 & h2 != 1]
      x1bad = x1[h1 == 1 & h2 != 1]
      plot(y1bad~x1bad,xlab=expression(log[10](AC[50])),ylab=paste0("Pathway Score ", metric),xlim=c(-3,3),
           ylim = c(-ylimit, ylimit),main=paste(cname,"\nDuplicate A"))
      x <- x1[h1==1 & h2 == 1]
      y <- y1[h1==1 & h2 == 1]
      points(y~x,pch=21,bg="red")
      lines(c(-5,5),c(0,0))
      lines(c(-5,5),c(bmad_factor,bmad_factor))
      lines(c(-5,5),c(-bmad_factor,-bmad_factor))
      legend("bottomleft", c("Hits not Present in Replicate","Hits in Both"),
             pch = c(21, 21), pt.bg = c("white", "red"))

      y2bad = y2[h1 != 1 & h2 == 1]
      x2bad = x2[h1 != 1 & h2 == 1]
      plot(y2bad~x2bad,xlab=expression(log[10](AC[50])),ylab=paste0("Pathway Score ", metric),xlim=c(-3,3),
           ylim = c(-ylimit, ylimit), main=paste(cname,"\nDuplicate B"))
      x <- x2[h1==1 & h2 == 1]
      y <- y2[h1==1 & h2 == 1]
      points(y~x,pch=21,bg="red")
      lines(c(-5,5),c(0,0))
      lines(c(-5,5),c(bmad_factor,bmad_factor))
      lines(c(-5,5),c(-bmad_factor,-bmad_factor))
      legend("bottomleft", c("Hits not Present in Replicate","Hits in Both"),
             pch = c(21, 21), pt.bg = c("white", "red"))


      }
    }
    
    # TOB_bothhit[[cname]] = c(y1[h1 & h2],y2[h1 & h2])
    # TOB_onehit[[cname]] = c(y1[h1 & !h2],y2[h2 & !h1])
    # TOB_bothhit_min[[cname]] = pmin(abs(y1[h1 & h2]), abs(y2[h1 & h2]))
    # TOB_bothhit_max[[cname]] = pmax(abs(y1[h1 & h2]), abs(y2[h1 & h2]))
    # DAC50_bothhit[[cname]] = abs(x1[h1 & h2] - x2[h1 & h2])
    
    mydf = data.frame(list(lac50_1 = x1, lac50_2 = x2, top1 = y1, top2 = y2, hit1 = h1, hit2 = h2))
    mydf = mydf[h1 | h2,] #don't need rows without any hits
    hitsdf[[cname]] = mydf
    
    # if(!to.file) browser()
  }
  
  # doubleDAC = unlist(lapply(DAC50_bothhit, function(x){rep(x,2)})) #rep to match with TOB_bothhit
  # bothhits = abs(unlist(TOB_bothhit))
  
  ha = Reduce(rbind,hitsdf)
  ha$lacdiff = abs(ha$lac50_1 - ha$lac50_2)
  ha$newhit1 = F
  ha$newhit2 = F  
  ha$rep = F
  ha$nonrep1 = F #non-replicated because only one hit top requirements
  ha$nonrep2 = F #non-replidated because both hit top req, but ac50's didn't match
  ha$hit1 = as.logical(ha$hit1)
  ha$hit2 = as.logical(ha$hit2)
  ha$lacdiff[is.na(ha$lacdiff)] = 9
  ha$top1[is.na(ha$top1)] = 0
  ha$top2[is.na(ha$top2)] = 0
  
  for(ACthresh in ACs){
    blen = 200
    # breaks = quantile(abs(c(ha$top1[ha$hit1], ha$top2[ha$hit2])),0:(blen-1)/blen)
    tops = abs(c(ha$top1[ha$hit1], ha$top2[ha$hit2]))
    breaks= seq(from = quantile(tops,0), to =quantile(tops,.99), length.out = blen )
    # breaks[1] = floor(breaks[1])
    # breaks[blen + 1] = ceiling(breaks[blen + 1])
    
    R = N = Rpath = Npath = breaks
    for(i in 1:length(breaks)){
      newbmad = breaks[i]
      ha$newhit1 = ha$hit1 & (abs(ha$top1) >= newbmad)
      ha$newhit2 = ha$hit2 & (abs(ha$top2) >= newbmad)
      ha$rep = ha$newhit1 & ha$newhit2 & (ha$lacdiff <= ACthresh)
      ha$nonrep1 = (ha$newhit1 & !ha$newhit2) | (!ha$newhit1 & ha$newhit2)
      ha$nonrep2 = (ha$newhit1 & ha$newhit2 & (ha$lacdiff > ACthresh))

      R[i] = 2*sum(ha$rep) #double count positives (replicated hits)
      N[i] = sum(ha$nonrep1) + 2*sum(ha$nonrep2) #double count nonrep2 because 2 nonreplicated hits

      Rpath[i] = sum(ha$rep) #single count positives (replicated pathways)
      Npath[i] = sum(ha$nonrep1) + sum(ha$nonrep2) #single count negative (nonreplicated pathways)
      
    }
    
    repscore = R/(R+N)*log(R+1)/log(max(R)+1)
    repscorepath = Rpath/(Rpath+Npath)*log(Rpath+1)/log(max(Rpath) + 1)
    
    bestloc = which.max(repscore)
    bestlocpath = which.max(repscorepath)    
    if(bmad_factor == "flpedisp") return(list(hitsdf = hitsdf, bestmetric = breaks[bestloc], bestmetricpath = breaks[bestlocpath],
      reprate = (R/(R+N))[bestloc], repratepath = (Rpath/(Rpath+Npath))[bestloc], R = R[bestloc], 
      Rpath = Rpath[bestloc], repscore = repscore[bestloc], repscorepath = repscorepath[bestloc]))
    
    uppery = max(c(R,N,Rpath,Npath))
    
    par(mar = c(5.1,4.1,4.1,4.1))
    plot(breaks, N, lwd = 2, type = "l", ylim = c(0,uppery), xlab = metric, ylab = "Number of Hits (black/red)", 
         main = paste0("AC50 Threshold: ", ACthresh))
    points(breaks, R, col = "Red", lwd = 2, type = "l")
    points(breaks, R/(N + R)*uppery, col = "blue", lwd = 2, type = "l")

    points(breaks, repscore*uppery, col = "darkorchid2", lwd = 2, type = "l")
    axis(side = 4, at = 0:4/4*uppery, labels = c(0,.25,.5,.75,1))
    mtext("Fraction(blue) or Score(purple)", side=4, line = 2, cex = .66)
    legend("top",c("Non-replicated", "Replicated", "Replicated Fraction", "Replication Score"),lwd = 2, 
           col = c("black", "red", "blue", "darkorchid2"))
    
    plot(breaks, Npath, lwd = 2, type = "l", ylim = c(0,uppery), xlab = metric, ylab = "Number of Pathways (black/red)", 
         main = paste0("AC50 Threshold: ", ACthresh))
    points(breaks, Rpath, col = "Red", lwd = 2, type = "l")
    points(breaks, Rpath/(Npath + Rpath)*uppery, col = "blue", lwd = 2, type = "l")
    points(breaks, repscorepath*uppery, col = "darkorchid2", lwd = 2, type = "l")
    axis(side = 4, at = 0:4/4*uppery, labels = c(0,.25,.5,.75,1))
    mtext("Fraction(blue) or Score(purple)", side=4, line = 2, cex = .66)
    legend("top",c("Non-replicated", "Replicated", "Replicated Fraction", "Replication Score"),lwd = 2, 
           col = c("black", "red", "blue", "darkorchid2"))
    
    # pos = bothhits[doubleDAC < ACthresh] #replicated hits
    # neg = c(abs(unlist(TOB_onehit)), bothhits[doubleDAC >= ACthresh]) #non-replicated hits
    # N = length(neg)
    # P = length(pos)
    # FP = sapply(breaks, function(x){sum(neg > x)})
    # TP = sapply(breaks, function(x){sum(pos > x)})
    # TN = sapply(breaks, function(x){sum(neg <= x)})
    # fpr = FP/N
    # tpr = TP/P
    
    # plot(fpr, tpr, type = "l", xlab = "False Positive Rate", ylab = "True Positive Rate", 
    #      main = paste0("AUC: ", signif(auc(tpr,fpr),2), ", AC50 Hit Threshhold: ", ACthresh), lwd = 2)
    # lines(c(0,1),c(0,1), lty = 2)
    # perfdist = sqrt((fpr)^2 + (1-tpr)^2)
    # bestloc = which.min(perfdist)
    # bestbmad = breaks[bestloc]
    # points(fpr[bestloc], tpr[bestloc],pch = 4, lwd = 2, col = "red", cex = 1.5)
    # text(fpr[bestloc], tpr[bestloc] + .05, labels = signif(bestbmad,2), col = "red")
    # otherlocs = sapply(c(2,3,5,10), function(x){which.min(abs(breaks-x))})
    # points(fpr[otherlocs], tpr[otherlocs],pch = 1, lwd = 2, col = "black", cex = 1.5)
    # otherbmads =  breaks[otherlocs]
    # text(fpr[otherlocs] - .02, tpr[otherlocs] + .05, labels = signif(otherbmads,2))
    # legend("bottomright", c("Closest Point to Perfect Classification","Other Selected BMADs"), pch = c(4,1), 
    #        col = c("red", "black"), pt.cex = 1.5, pt.lwd = 2)
    
    # poscol = rgb(1,0,0,.5)
    # negcol = rgb(.5,.5,.5,.5)
    # hist(N, breaks = 100, col = negcol,
    #      xlab = "Pathway Score Top Over BMAD (For Pathways That Hit)", ylab = "Count (Overlapping Histograms)",
    #      main = paste0("AC50 Threshhold: ", ACthresh))
    # hist(R, add = T, breaks = 100, col = poscol)
    # legend("topright",c("Replicated Pathways","Non-replicated Pathways"), fill = c(poscol, negcol))
    
    # hist(neg, breaks = 100, ylim = c(0,10000), col = negcol, 
    #      xlab = "Pathway Score Top Over BMAD (For Pathways That Hit)", ylab = "Count (Overlapping Histograms)", 
    #      main = paste0("AC50 Threshhold: ", ACthresh))
    # hist(pos, add = T, breaks = 100, col = poscol)
    # legend("topright",c("Replicated Pathways","Non-replicated Pathways"), fill = c(poscol, negcol))
    
    # plot(breaks, TP/P, type = "l", lwd = 2, xlab = "Pathway Score Top Over BMAD", 
    #      ylab = "Fraction of Hits", main = paste0("AC50 Threshhold: ", ACthresh), ylim = c(0,1))
    # points(breaks, TP/(TP + FP),type = "l", lwd = 2, col = "red")
    # points(breaks, .5*(TP/P + TN/N),type = "l", lwd = 2, col = "blue")
    # 
    # legend("bottomright",c(paste0("TP/P, P = ", P),"TP/(TP + FP)", 
    #                     "Balanced Accuracy"),lwd = 2, col = c("black", "red", "blue"))
    # 
    # bestloc = which.max(.5*(TP/P + TN/N))
    # points(breaks[bestloc], (.5*(TP/P + TN/N))[bestloc], pch = 4, lwd = 2, cex = 1.5, col = "blue")
    # text(breaks[bestloc]+.5, (.5*(TP/P + TN/N))[bestloc]+.05, labels = signif(breaks[bestloc],2), col = "blue")
    # 
    # plot(breaks, TP/P, type = "l", lwd = 2, xlab = "Pathway Score Top Over BMAD", 
    #      ylab = "Fraction of Hits", main = paste0("AC50 Threshhold: ", ACthresh), ylim = c(0,1))
    # points(breaks, TP/(TP + FP),type = "l", lwd = 2, col = "red")
    # points(breaks, .5*(TP/P + TN/N),type = "l", lwd = 2, col = "blue")
    # 
    # legend("bottomright",c(paste0("TP/P, P = ", P),"TP/(TP + FP)", 
    #                        "Balanced Accuracy"),lwd = 2, col = c("black", "red", "blue"))
    # 
    # bestloc = which.max(.5*(TP/P + TN/N))
    # points(breaks[bestloc], (.5*(TP/P + TN/N))[bestloc], pch = 4, lwd = 2, cex = 1.5, col = "blue")
    # text(breaks[bestloc]+.5, (.5*(TP/P + TN/N))[bestloc]+.05, labels = signif(breaks[bestloc],2), col = "blue")
    
    # boxplot(doubleDAC ~ signif(bothhits,1), xlab = "BMAD/top of Hit", 
    #         ylab = "Delta AC50")
    # 
    # tob = bothhits[order(bothhits)]
    # DAC50 = doubleDAC[order(bothhits)]
    # n = 500
    # index = (n+1):(length(tob)-n)
    # ACrmse = sapply(index, function(x){rmse(DAC50[(x-n):(x+n)])})
    # plot(tob[index], ACrmse, type = "l", ylab = paste0("Running RMSE of AC50s with Bandwidth: ", 2*n+1), 
    #      xlab = "BMAD/top of Hit", ylim = c(0,3))
    # lines(c(1,30), c(1,1), lty = 2)
    
    # ACmae = sapply(index, function(x){mean(DAC50[(x-n):(x+n)])})
    # plot(tob[index], ACmae, type = "l", ylab = paste0("Running Average of |Delta AC50| with Bandwidth: ", 2*n+1),
    #      xlab = "BMAD/top of Hit")
    # # points(tob[index], ACmae, type = "l", col = "red")
    # lines(c(0,30), c(.5,.5), lty = 2)
    # lines(c(2.5,2.5), c(0,3), lty = 2)
  
  }
  
  #Relationship b/t AC50 and replication
  # ACthresh = 1
  # ACbreaks = -3:3
  # k = length(ACbreaks)
  # newbmads = 2:9
  # ha$lac50_1[is.na(ha$lac50_1)] = Inf
  # ha$lac50_2[is.na(ha$lac50_2)] = Inf
  # Rmat = matrix(ncol = length(newbmads), nrow = k - 1)
  # rownames(Rmat) = ACbreaks[1:(k-1)]
  # colnames(Rmat) = newbmads
  # Nmat = reprates = Rmat
  # for(j in 1:length(newbmads)){
  #   newbmad = newbmads[j]
  #   ha$newhit1 = ha$hit1 & (abs(ha$top1) >= newbmad)
  #   ha$newhit2 = ha$hit2 & (abs(ha$top2) >= newbmad)
  #   ha$rep = ha$newhit1 & ha$newhit2 & (ha$lacdiff <= ACthresh)
  #   ha$nonrep1 = (ha$newhit1 & !ha$newhit2) | (!ha$newhit1 & ha$newhit2)
  #   ha$nonrep2 = (ha$newhit1 & ha$newhit2 & (ha$lacdiff > ACthresh))
  #   
  #   for(i in 1:(k - 1)){
  #     AC1 = ACbreaks[i]
  #     AC2 = ACbreaks[i+1]
  #     Rmat[i,j] = sum(ha$rep[ha$lac50_1 >= AC1 & ha$lac50_1 < AC2]) + sum(ha$rep[ha$lac50_2 >= AC1 & ha$lac50_2 < AC2])
  #     Nmat[i,j] = sum(ha$nonrep1[ha$lac50_1 >= AC1 & ha$lac50_1 < AC2]) + sum(ha$nonrep1[ha$lac50_2 >= AC1 & ha$lac50_2 < AC2]) +
  #         sum(ha$nonrep2[ha$lac50_1 >= AC1 & ha$lac50_1 < AC2]) + sum(ha$nonrep2[ha$lac50_2 >= AC1 & ha$lac50_2 < AC2])
  # 
  #   }
  # 
  # }
  # reprates = Rmat/(Rmat+Nmat)
  # 
  # cols =  rainbow(k-1, start = .3, end = .85)
  # 
  # barplot(Rmat, beside = T, xlab = "Top/BMAD", ylab = "Replicated Hits", col = cols)
  # legend("topright", legend = paste0("AC50 ",ACbreaks[1:(k-1)], " to ", ACbreaks[2:k]),fill = cols, ncol = 2)
  # barplot(Nmat, beside = T, xlab = "Top/BMAD", ylab = "Non - Replicated Hits", col = cols)
  # legend("topright", legend = paste0("AC50 ",ACbreaks[1:(k-1)], " to ", ACbreaks[2:k]),fill = cols, ncol = 2)
  # barplot(reprates, beside = T, xlab = "Top/BMAD", ylab = "Replication Rate", col = cols, ylim = c(0, 1.3* max(reprates)))
  # legend("topleft", legend = paste0("AC50 ",ACbreaks[1:(k-1)], " to ", ACbreaks[2:k]),fill = cols, ncol = 2)
  # 
  if(to.file) dev.off()
}

rmse = function(x){
  return(sqrt(mean(x^2)))
}

auc = function(tpr, fpr){
  auc = 0
  for(i in 1:(length(tpr)-1)){
    auc = auc + (tpr[i] + tpr[i+1])*(fpr[i+1]-fpr[i])/2
  }
  return(abs(auc))
}