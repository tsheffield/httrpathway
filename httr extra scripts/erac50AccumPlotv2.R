source("R/general_utils.R")
source("R/hitlogic.R")
library(openxlsx)

#accumulation plots for the hepaRG candidates; now with null distributions shown on the plot

erac50AccumPlot = function(pathset="bhrr", dataset="arer", methods = c("fc", "gsva", "mygsea"),
                                   potency = "ac50", newbmads = c(2,2,2)){
  
  printCurrentFunction()
  
  N = length(methods)
  
  #get pathway_Crs from regular data
  hitmats = lapply(as.list(1:N), function(i){
    method = methods[i]
    file <- paste0("output/pathway_conc_resp_summary/PATHWAY_CR_",pathset,"_",dataset,"_",method ,"_0.2.RData")
    load(file)
    if(any(newbmads < 1)){
      pvalkey = getpvalcutoff(pathset, nullset = paste0(dataset, "_RAND1000"), method = method, pvals = newbmads[i])
      newcutoff = pvalkey$cutoff[match(PATHWAY_CR$pathway, pvalkey$pathway)]
      PATHWAY_CR$hitcall = hitlogic(PATHWAY_CR, newcutoff = newcutoff)
    } else {
      PATHWAY_CR$hitcall = hitlogic(PATHWAY_CR,newbmads[i])
    }
    
    PATHWAY_CR$method = rep(method, nrow(PATHWAY_CR))
    return(PATHWAY_CR[PATHWAY_CR$hitcall == 1,])
  })
  hitmat = Reduce(rbind, hitmats)
  
  #get matching pway info
  genekey = read.xlsx("Gene Keywords.xlsx")
  
  #get er ac50s
  erscores = read.xlsx("S2 ER SuperMatrix 2015-03-24.xlsx")
  hitmat$erac50 = erscores$pseudo.AC50.median[match(hitmat$casrn ,erscores$CASRN)]
  ercasrns = erscores$CASRN[erscores$AUC.Agonist >= .1 | erscores$AUC.Antagonist >= .1]
  hitmat = hitmat[hitmat$casrn %in% ercasrns,]
  
  #choose mie AR or ER
  arscores = read.xlsx("AR_supermatrix_Kleinstreuer_etal_2017.xlsx")
  erscores$maxauc = pmax(erscores$AUC.Agonist, erscores$AUC.Antagonist)
  ers = erscores$CASRN[erscores$maxauc >= .1]
  arscores$maxauc = pmax(arscores$AUC.Agonist, arscores$AUC.Antagonist)
  ars = arscores$CASRN[arscores$maxauc >= .1]
  boths = intersect(ers,ars)
  rownames(erscores) = erscores$CASRN
  rownames(arscores) = arscores$CASRN
  ers = setdiff(ers, boths[erscores[boths, "maxauc"] < arscores[boths, "maxauc"]] )
  ars = setdiff(ars, boths[erscores[boths, "maxauc"] >= arscores[boths, "maxauc"]] )
  
  #rename potency column, eliminate nas
  hitmat$potency = hitmat[,potency]
  hitmat = hitmat[!is.na(hitmat$potency),]
  
  #get bottom, top, chems, all unique points for later
  bottom = floor(min(log10(hitmat$potency ) ) )
  top = ceiling(max(log10(hitmat$potency ) ) )
  chems = unique(hitmat$sample_id[order(hitmat$name)])

  pts = sort(unique(signif(hitmat$potency,2)))
  #set colors
  cols = c("black", "blue", "red")
  
  #open pdf
  file = paste0("output/accumplots/erac50ACCUMPLOT_",pathset,"_",dataset,"_",potency,"_", paste0(newbmads, collapse = "_"),".pdf")
  pdf(file= file,width=8,height=10,pointsize=12,bg="white",paper="letter",pagecentre=T)
  par(mfrow=c(3,2),mar=c(4,4,2,2))

  #plotting loops, outer is chems
  out = lapply(as.list(chems), function(x){
  
    chemhits = hitmat[hitmat$sample_id == x,]
    myname = chemhits$name[1]
    mycasrn = chemhits$casrn[1]
    if(mycasrn %in% ers) mie = "ER" else if(mycasrn %in% ars) mie = "AR"
    
    allpways = unlist(strsplit(genekey$Curated.Pathways[genekey$Class.Label == "ER"], "\\|"))
    #get max number of hits for a chem
    chemhits$potlog = !is.na(chemhits$potency)
    maxhits = max(aggregate(potlog ~ method ,chemhits, sum)$potlog)
    #empty plot
    plot(0, type = 'n', xlim = c(10^bottom, 10^top), ylim = c(0,maxhits), log = "x", 
         ylab = "Pathway Hits (Matching Pathways Highlighted)", xlab = potency, main = paste0(myname, " (",mie,")"))
    #method loop
    for(i in 1:N){
      mymat = chemhits[chemhits$method == methods[i],]
      
      if(nrow(mymat) > 0){
        #get and plot ecdf
        myecdf = ecdf(mymat$potency)
        ys = myecdf(pts)*nrow(mymat)
        points(pts, ys, type = "l", col = cols[i])
        
        #add matching pway points
        matchconcs = unique(signif(mymat$potency[mymat$name == myname & mymat$pathway %in% allpways],2))
        matchys = myecdf(matchconcs)*nrow(mymat)
        points(matchconcs, matchys, col = cols[i], pch = 16)

      }
      
    }
    erac50 = mymat$erac50[1]
    
    lines(c(erac50,erac50),c(0,maxhits), col = "black", lty = 2)
    
    legend("topleft", legend = methods, col = rep(cols,2), cex = .8, lty = 1, pch = 16)
  })
  dev.off()
  
  # return(hitmat[hitmat$ac50 < hitmat$erac50,])
  
}