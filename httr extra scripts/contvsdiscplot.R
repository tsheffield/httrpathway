conthitvsdiscplot = function() {
  # dhits = dpcr$hitcall
  # # chits = cpcr$hitcall
  # chits = hits3*hits1*hits2
  # chits[chits < .2] = rescaler(chits[chits < .2], 0, .2, 0, .01)
  # chits[chits > .55] = rescaler(chits[chits > .55], .55, 1, .99, 1)
  # chits[chits <= .55 & chits >= .2] = rescaler(chits[chits <= .55 & chits >= .2], .2, .55, .01, .99)
  
  ints = 100
  cs = sort(chits)
  breaks = cs[seq(1,length(cs), length.out = ints)]
  
  # breaks = 0:50/50
  output = rep(0,length(breaks)-1)
  mids = ls = rep(0,length(breaks)-1)
  for(i in 1:(length(breaks)-1)){
    output[i] = mean(dhits[chits >= breaks[i] & chits < breaks[i+1]])
    mids[i] = mean(breaks[i:(i+1)])
    ls[i] = length(dhits[chits >= breaks[i] & chits < breaks[i+1]])
  }
  plot(mids, output, xlab = "Continous Hitcall", ylab = "Fraction Dicrete Hits", 
       main = "Estrogen Actives/Inactives, pvalue = .05")
}

rescaler = function(x, a, b, ap, bp){
  return(  (x-a)*(bp-ap)/(b-a) + ap  )
  
  
}