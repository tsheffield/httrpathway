library(gplots)

pathwayOverlap = function(pathset = "bhrr"){
  
  file = paste0("input/processed_pathway_data/PATHWAY_LIST_",pathset,".RData")
  load(file)
  
  comparepaths = c("HALLMARK_ESTROGEN_RESPONSE_EARLY", "HALLMARK_ESTROGEN_RESPONSE_LATE", 
                "DUTERTRE_ESTRADIOL_RESPONSE_6HR_UP","RYAN_ESTROGEN_RECEPTOR_ALPHA",
                "Androgen receptor regulation of biosynthesis and transcription", 
                "Androgen receptor proteolysis and transcription regulation", 
                "HALLMARK_ANDROGEN_RESPONSE", "WANG_RESPONSE_TO_ANDROGEN_UP")
  
  pathgenes = pathway_data[comparepaths]
  commongenes = unique(unlist(pathgenes))
  
  heatmat = matrix(NA_integer_, ncol = length(commongenes), nrow = length(comparepaths))
  rownames(heatmat) = comparepaths
  colnames(heatmat) = commongenes
  
  for(path in comparepaths){
    heatmat[path,] = as.integer(commongenes %in% pathgenes[[path]])
  }
  
  rownames(heatmat) = tolower(strtrim(rownames(heatmat),40))
  heatmap.2(heatmat, trace = "none", margins = c(4,14), cexCol = 0.4, cexRow = .8, breaks = c(-.001, .5, 1.001), 
            col = c("white", "black"), distfun = function(x){dist(x,method = "binary")})
  
  dists = as.matrix(dist(heatmat, method = "binary"))
  
  heatmap.2(dists, trace = "none", margins = c(14,14), cexCol = 0.8, cexRow = .8, main = "Tanimoto Distance")
}