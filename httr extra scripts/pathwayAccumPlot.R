source("R/general_utils.R")
source("R/hitlogic.R")
library(openxlsx)

pathwayAccumPlot = function(pathset="REACTHALL", dataset="Pilot44_6", methods = c("fc", "gsva", "mygsea"),
                                   potency = "ac3bmad", newbmads = c(2.5,2,2)){
  
  printCurrentFunction()
  
  N = length(methods)
  
  hitmats = lapply(as.list(1:length(methods)), function(i){
    method = methods[i]
    file <- paste0("output/pathway_conc_resp_summary/PATHWAY_CR_",pathset,"_",dataset,"_",method ,"_1.RData")
    load(file)
    PATHWAY_CR$hitcall = hitlogic(PATHWAY_CR,newbmads[i])
    
    PATHWAY_CR$method = rep(method, nrow(PATHWAY_CR))
    return(PATHWAY_CR[PATHWAY_CR$hitcall == 1,])
  })
  
  
  hitmat = Reduce(rbind, hitmats)
  chemclasses = read.xlsx("input/chemicals/httr_chemicals.xlsx")
  hitmat$expected_class = chemclasses$pathway_class[match(hitmat$name, chemclasses$chem)]
  
  colnames(hitmat)[colnames(hitmat) == potency] = "potency"
  keepcols = c("name", "pathway", "expected_class", "pathway_class", "pathway_size","potency", "method")
  hitmat = hitmat[,keepcols]

  
  # n = 5
  chems = sort(unique(hitmat$name))
  # ordered = hitmat[order(hitmat$potency),]
  # out = sapply(chems, function(x){
  #   whichs = which(ordered$name == x)
  #   whichs[1:min(c(length(whichs), n))]
  # })
  # outmat = ordered[unlist(out),]
  # 
  # file = file = paste0("output/pathway_conc_resp_summary/HITSUMMARY_",pathset,"_",dataset,"_",method,".xlsx")
  # write.xlsx(outmat, file = file)
  
  
  file = paste0("output/accumplots/ACCUMPLOT_",pathset,"_",dataset,"_",potency,".pdf")
  pdf(file= file,width=8,height=10,pointsize=12,bg="white",paper="letter",pagecentre=T)
  par(mfrow=c(3,2),mar=c(4,4,2,2))
  
  # bottom = floor(min(log10(hitmat$potency)))
  # top = ceiling(max(log10(hitmat$potency)))
  # pts = 10^(seq(bottom, top, .01))
  cols = c("black", "blue", "red", "darkgreen", "purple", "orange")[1:N]
  legendvec = rep("a",N)
  out = lapply(as.list(chems), function(x){
    chemhits = hitmat[hitmat$name == x,]
    # bottom = floor(min(log10(chemhits$potency[!is.na(chemhits$potency)] ) ) )
    # top = ceiling(max(log10(chemhits$potency[!is.na(chemhits$potency)] ) ) )
    bottom = floor(min(log10(chemhits$potency[!is.na(chemhits$potency)] ) ) )
    top = ceiling(quantile(log10(chemhits$potency[!is.na(chemhits$potency)] ), .99 ) )
    pts = 10^(seq(bottom, top, .01))
    chemhits$potency = !is.na(chemhits$potency)
    maxhits = max(aggregate(potency ~ method ,chemhits, sum)$potency)
    plot(0, type = 'n', xlim = c(10^bottom, 10^top), ylim = c(0,maxhits), log = "x", ylab = "Number of Pathway Hits at Given Concentration", 
         xlab = potency, main = x)
    for(i in 1:N){
      mymat = hitmat[hitmat$method == methods[i],]
      hitconcs = mymat$potency[mymat$name == x]
      if(length(hitconcs) > 0){
        myecdf = ecdf(hitconcs)
        points(pts,myecdf(pts)*length(hitconcs), type = "l", col = cols[i])
        quant05 = quantile(hitconcs,.05)
        lines(c(quant05,quant05),c(-.02*maxhits,.25*maxhits), col = cols[i])
        legendvec[i] = paste0(methods[i], "\n5%: ", signif(quant05, digits = 3), "\n")
        # legendvec[i] = paste0("Method: ", methods[i], "\n5% Quantile: ", signif(quant05, digits = 3), "\n")
      } else {
        legendvec[i] = paste0(methods[i])
      }
      
                            # "\nMin: ", signif(min(hitconcs), digits = 3), 
                           # "\nMedian: ", signif(median(hitconcs), digits = 3), "\n")
    }
    legend("topleft", legend = legendvec, lwd = 2, col = cols)
  })
  graphics.off()
  
}