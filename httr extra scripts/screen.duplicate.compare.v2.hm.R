source("R/general_utils.R")
library(openxlsx)
#--------------------------------------------------------------------------------------
#' Compare the data from duplicates at the gene level
#' @param dir The directory where the analyses will be put
#' @param dataset1 The name of the first data set
#' @param dataset2 The name of the second data set
#' @param method Either gene or probe_id
#' @param to.file If TRUE, plot the data to a pdf file
#'
#' @return Nothing is returned
#' Two files are generated, a pdf of the plots and an Excel of the fit parameters
#'
#--------------------------------------------------------------------------------------
screen.duplicate.compare.v2.hm <- function(to.file=F,outdir="analysis/screen_duplicate_compare_v2/") {
  printCurrentFunction()
  method.list <- c("meanncnt0_5-plateteffect_0-dispest_parametric-betaprior_0",
                   "meanncnt0_5-plateteffect_0-dispest_parametric-betaprior_1",
                   "meanncnt0_5-plateteffect_1-dispest_parametric-betaprior_0",
                   "meanncnt0_5-plateteffect_1-dispest_parametric-betaprior_1",
                   "meanncnt0_10-plateteffect_0-dispest_parametric-betaprior_0",
                   "meanncnt0_10-plateteffect_0-dispest_parametric-betaprior_1",
                   "meanncnt0_10-plateteffect_1-dispest_parametric-betaprior_0",
                   "meanncnt0_10-plateteffect_1-dispest_parametric-betaprior_1"
  )
  
#  method.labels <- c("plteff(-) 5 parametric",
#                     "plteff(-) 5 mean",
#                     "plteff(+) 5 parametric",
#                     "plteff(+) 5 mean",
#                     "plteff(-) 10 parametric",
#                     "plteff(-) 10 mean",
#                     "plteff(+) 10 parametric",
#                     "plteff(+) 10 mean")
#  
  
  if(to.file) {
    fname <- paste0(outdir,"screen_duplicate_compare_v2_hm.pdf")
    pdf(file=fname,width=8,height=10,pointsize=12,bg="white",paper="letter",pagecentre=T)
  }
 
  file <- paste0(outdir,"screen_duplicate_compare_v2.xlsx")
  res <- read.xlsx(file)
  
  res$rowkey <- paste0(res$chem,"_",res$iconc)
  res2 <- res[,c("rowkey","method","mad")]
  mat <- dcast(res2,rowkey~method)
  rownames(mat) <- mat$rowkey
  mat2 <- mat[,2:ncol(mat)]
  
  name.list <- names(mat2)
  for(i in 1:length(name.list)) {
    x <- name.list[i]
    x <- str_replace(x,"meanncnt0_","MC ")
    x <- str_replace(x,"-plateteffect_"," PE ")
    x <- str_replace(x,"-dispest_parametric-betaprior_"," AH ")
    name.list[i] <- x
  }
  names(mat2) <- name.list
  heatmap.2(as.matrix(mat2),
            Rowv=T,
            Colv=T,
            hclustfun=function(x) hclust(d=dist(x),method="ward.D"),
            dendrogram="both",
            symm=F,
            scale="none",
            col=brewer.pal(9,"Reds"),
            margins=c(10,8),
            cexCol=0.6,
            cexRow=0.2,
            key=T,
            main="MAD by method",
            xlab="",
            ylab="",
            trace="none",
            key.title="Key",
            key.xlab="MAD",
            cex.main=1)
  
  if(to.file) dev.off()
}