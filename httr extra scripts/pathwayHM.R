library(gplots)
library(reshape2)

pathwayHMsave = function(pathset = "ALL", dataset = "Pilot44_6", methods = c("fc","gsva"), vvars = c("pathway_score", "efficacy"),
                         maxconcs = c(30,100)){
  file = paste0("output/pathway_score_summary/heatmaps_",pathset,"_",dataset,"_",paste0(methods, collapse = "_") ,".pdf")
  pdf(file= file,width=10,height=5.5,pointsize=7,bg="white",pagecentre=T)
  
  for(method in methods){
    file <- paste0("output/pathway_conc_resp_summary/PATHWAY_CR_",pathset,"_",dataset,"_",method ,".RData")
    load(file)
    file <- paste0("output/pathway_score_summary/PATHSCOREMAT_",pathset,"_",dataset,"_",method ,".RData")
    load(file)
    for(vvar in vvars){
      for(maxconc in maxconcs){
         pathwayHMv2(pathscoremat = pathscoremat, PATHWAY_CR = PATHWAY_CR, vvar = vvar, maxconc = maxconc,
                     main = paste0("Method: ", method, ", Maxconc: ", maxconc, ", \nValue: ", vvar))
      }
    }  
  }
  
  graphics.off()
  
}

pathwayHMv2 = function(pathscoremat = NULL, PATHWAY_CR = NULL, pathset = "ALL", dataset = "Pilot44_6", method = "fc", npways = 100, 
                       seed = 12345678, nbreaks = 51, vvar = "efficacy", maxconc = 100, main = NULL){
  
  if(nbreaks%%2 != 1) nbreaks = nbreaks + 1
  set.seed(seed)
  if(is.null(pathscoremat)){
    file <- paste0("output/pathway_score_summary/PATHSCOREMAT_",pathset,"_",dataset,"_",method ,".RData")
    load(file)
  }
  if(is.null(PATHWAY_CR)){
    file <- paste0("output/pathway_conc_resp_summary/PATHWAY_CR_",pathset,"_",dataset,"_",method ,".RData")
    load(file)
  }
  
  selectpways = c("DUTERTRE_ESTRADIOL_RESPONSE_6HR_UP",
                  "FRASOR_RESPONSE_TO_ESTRADIOL_UP", "MASSARWEH_RESPONSE_TO_ESTRADIOL", "STOSSI_RESPONSE_TO_ESTRADIOL",
                  "HALLMARK_ESTROGEN_RESPONSE_EARLY", "STEIN_ESTROGEN_RESPONSE_NOT_VIA_ESRRA")
  
  selectchems = c("4-Hydroxytamoxifen", "Clomiphene citrate (1:1)", "Fulvestrant","4-Cumylphenol", "4-Nonylphenol, branched", 
                  "Bisphenol A", "Bisphenol B")
  
  pways = unique(pathscoremat$pathway)
  extrapways = sample(pways[!pways %in% selectpways], npways)

  allpways = sample(c(extrapways, selectpways))
  
  pathmat = pathscoremat[pathscoremat$pathway %in% allpways,]
  pathmat = pathmat[pathmat$name %in% selectchems,]
  pathmat = pathmat[pathmat$conc <= maxconc,]
  
  crmat = PATHWAY_CR[PATHWAY_CR$pathway %in% allpways,]
  crmat = crmat[crmat$name %in% selectchems,]
  
  doublematch = match(paste0(pathmat$pathway,pathmat$name), paste0(crmat$pathway,crmat$name))
  pathmat$top = crmat$top[doublematch]
  pathmat$efficacy = pathmat$top
  pathmat$efficacy[pathmat$efficacy != 0] = pathmat$pathway_score[pathmat$efficacy != 0]/abs(pathmat$top[pathmat$efficacy != 0])
  
  scoremat = acast(pathmat, name + conc ~ pathway, value.var = vvar)
  
  colnames(scoremat) = strtrim(colnames(scoremat),30)
  
  nconcs = length(unique(pathmat$conc))
  seps = rowsep = seq(nconcs,nrow(scoremat), by = nconcs)
  
  absmax = max(abs(scoremat))
  
  quant999 = max(abs(quantile(scoremat,c(.001,.999))))
  
  mybreaks = c(-absmax,seq(-quant999,quant999, length.out = nbreaks-2), absmax)

  heatmap.2(scoremat, trace = "none", margins = c(15,10), Rowv = F, dendrogram = "column", rowsep = seps, sepcolor = "black",
            col = bluered, breaks = mybreaks, main = main)
}