library(openxlsx)
source("R/general_utils.R")
#--------------------------------------------------------------------------------------
#' plot the number of pathway hits as a function of concentration
#'
#' @param to.file It TRUE, make a pdf with the plots
#' @param pathset The name of the pathway set, currently either Reference_44 of ALL
#' @param method The pathway scoring method
#' @return
#'
#--------------------------------------------------------------------------------------
pathwayConcRespSummaryPlot <- function(to.file=F,pathset,method="fc",version="v1") {
  printCurrentFunction()
  if(!exists("PATHWAY_CR")) {
    file <- paste0("output/pathway_conc_resp_summary/pathway_concresp_",pathset,"_",method,"_",version,".RData")
    load(file)
    PATHWAY_CR <<- PATHWAY_CR
 	}
 	mat <- PATHWAY_CR
  cat("PATHWAY_CR read in ...\n")
	#
	# Next run by chemical
	#
  if(to.file) {
	  fname <- paste0("output/pathway_conc_resp_summary/pathway_conc_resp_summary_by_chemical_",pathset,"_",method,"_",version,".pdf")
     pdf(file=fname,width=8,height=10,pointsize=12,bg="white",paper="letter",pagecentre=T)
  }
  par(mfrow=c(5,3),mar=c(4,4,2,2))
  xmin <- 0.01
  xmax <- 100
  ymin <- -2
  ymax <- 2
  chemset <- sort(unique(mat[,"name"]))
  nchem <- length(chemset)
  for(chem in chemset) {
   	cat(chem,"\n")
   	temp <- mat[is.element(mat[,"name"],chem),]
   	temp <- temp[temp[,"hitcall"]==1,]
   	# browser()
    sid <- temp[1,"sample_id"]
    xlist <- sort(unique(pathscoremat[is.element(pathscoremat[,"sample_id"],sid),"conc"]))
    ylist <- xlist
    ylist[] <- 0
    
    x <- temp[,"ac3bmad"]
    y <- temp[,"pathway_hits"]
    for(i in 1:length(x)) {
      xi <- x[i]
      index <- which.max(xlist==xi)
      ylist[index] <- y[i]
    }
    ylist2 <- ylist
    ylist2[] <- 0
    ylist2[1] <- ylist[1]
    for(i in 2:length(ylist2)) ylist2[i] <- ylist2[i-1]+ylist[i]
    plot(ylist2~xlist,xlim=c(xmin,xmax),ylim=c(ymin,ymax),main=chem,xlab="AC50(uM)",ylab="Top",cex.lab=1.2, cex.axis=1.2,type="l",log="x",lwd=2)
    if(!to.file) browser()
  }
  if(to.file) dev.off()
}
