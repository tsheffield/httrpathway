source("R/general_utils.R")
library(openxlsx)
#--------------------------------------------------------------------------------------
#' Compare the data from duplicates at the gene level
#' @param dir The directory where the analyses will be put
#' @param method Either gene or probe_id
#' @param to.file If TRUE, plot the data to a pdf file
#'
#' @return Nothing is returned
#' Two files are generated, a pdf of the plots and an Excel of the fit parameters
#'
#--------------------------------------------------------------------------------------
pathwayDuplicateComparel2fc <- function(dir="output/screen_duplicate_compare/",
                                   cutoff=2,
                                   pathset = "ALL",
                                   dataset = "Phase1_6reps",
                                   method = "fc",
                                   to.file=F) {
  printCurrentFunction()
  pathdatamethod = paste0(pathset, "_", dataset, "_", method)
  if(to.file) {
    fname <- paste0(dir,"duplicate_compare_",cutoff, "_", pathdatamethod,".pdf")
    pdf(file=fname,width=8,height=10,pointsize=12,bg="white",paper="letter",pagecentre=T)
  }
  par(mfrow=c(3,2),mar=c(4,4,2,2))
  
  file = paste0("output/pathway_conc_resp_summary/PATHWAY_CR_", pathdatamethod, ".RData")
  load(file)
  
  cnames = unique(PATHWAY_CR$name)
  cnames = cnames[cnames != "Genistein"] #remove Genistein
  
  #create custom bin.list based on quantiles of whole data abs(top)
  # quants = quantile(abs(PATHWAY_CR$top[!is.na(PATHWAY_CR$ac50)]), (1:10/10))
  # quants = unique(round(quants,2))
  # bin.list = sort(c(-quants, 0, quants))
  # bin.list = quantile((PATHWAY_CR$top[!is.na(PATHWAY_CR$ac50)]), (0:19/20 + .025))
  step = quantile((PATHWAY_CR$top[!is.na(PATHWAY_CR$ac50)]), .95)
  step= signif(q/9,1)
  bin.list = -9:9*step
  
  for(cname in cnames) {
    cat(cname,"\n")
    data = PATHWAY_CR[PATHWAY_CR$name == cname,]
    samples = unique(data$sample_id)
    if(length(samples) == 1) {
      print("Chemical not replicated")
      break
    }
    if(length(samples) > 2) {
      print("Three or more replicates: using first two")
      samples = sample[1:2]
    }
    
    data1 = data[data$sample_id == samples[1],]
    data2 = data[data$sample_id == samples[2],]
    rm(data)
    rownames(data1) <- data1[,"pathway"]
    rownames(data2) <- data2[,"pathway"]
    n1 <- nrow(data1)
    n2 <- nrow(data2)
    path.list <- data1[,"pathway"]
    path.list <- path.list[is.element(path.list,data2[,"pathway"])]
    path.list <- sort(path.list)
    n12 <- length(path.list)
    data1 <- data1[path.list,]
    data2 <- data2[path.list,]
    
    #plot(c(1,1),main=cname,xlim=c(1e-3,100),ylim=c(-5,5),log="x",xlab="log(conc)",ylab="log2FC",type="n")
    #lines(c(1e-6,0),c(1e6,0))
    x1 <- log10(data1[,"ac50"])
    y1 <- data1[,"top"]
    y1[is.na(x1)] <- NA
    h1 <- data1[,"hitcall"]
    x2 <- log10(data2[,"ac50"])
    y2 <- data2[,"top"]
    y2[is.na(x2)] <- NA
    h2 <- data2[,"hitcall"]
    delta.pot <- x1
    delta.pot[] <- NA
    extreme <- delta.pot
    replicate.data <- delta.pot
    replicate.hit <- delta.pot
    replicate.gt.cutoff <- delta.pot
    replicate.data[] <- 0
    replicate.hit[] <- 0
    replicate.gt.cutoff[] <- 0
    for(i in 1:length(path.list)) {
      if(!is.na(x1[i]) && !is.na(y1[i]) && !is.na(x2[i]) &!is.na(y2[i])) replicate.data[i] <- 1
      if(h1[i]==1 && h2[i]==1) replicate.hit[i] <- 1
      if(replicate.data[i]==1) {
        maxval <- max(abs(y1[i]),abs(y2[i]))
        if(maxval==abs(y1[i])) extreme[i] <- y1[i]
        else if(maxval==abs(y2[i])) extreme[i] <- y2[i]
        delta.pot[i] <- abs(x1[i]-x2[i])
      }
      if(!is.na(y1[i]) && !is.na(y2[i]) ) {
        if(abs(y1[i])>cutoff && abs(y2[i])>cutoff) replicate.gt.cutoff[i] <- 1
        else if(abs(y1[i])>cutoff || abs(y2[i])>cutoff) replicate.gt.cutoff[i] <- -3
      }
      else if(!is.na(y1[i])) {
        if(abs(y1[i])>cutoff) replicate.gt.cutoff[i] <- -1
      }
      else if(!is.na(y2[i])) {
        if(abs(y2[i])>cutoff) replicate.gt.cutoff[i] <- -2
      }
    }
    
    mat <- cbind(x1,x2,y1,y2,h1,h2,replicate.gt.cutoff)
    if(length(delta.pot)>2) {
      bins <- extreme
      bins[] <- 0
      # bin.list <- c(-10,-6,-4,-3.5,-3,-2.5,-2,-1.5,-1,0,1,1.5,2,2.5,3,3.5,4,6,10)
      for(i in 1:length(path.list)) {
        x <- extreme[i]
        if(!is.na(x)) bins[i] <- bin.list[which.min(abs(bin.list-x))]
      }
      nboth.gt.cutoff <- length(replicate.gt.cutoff[replicate.gt.cutoff==1])
      n1.gt.cutoff <- length(replicate.gt.cutoff[replicate.gt.cutoff==-1])
      n2.gt.cutoff <- length(replicate.gt.cutoff[replicate.gt.cutoff==-2])
      n3.gt.cutoff <- length(replicate.gt.cutoff[replicate.gt.cutoff==-3])
      count.list <- bin.list
      count.list[] <- 0
      for(i in 1:length(bin.list)) {
        bin <- bin.list[i]
        count.list[i] <- sum((bins==bin) &  !is.na(delta.pot)) #now disregards NAs
      }
      if(sum(!is.na(delta.pot)) > 0){
        boxplot(delta.pot~bins,ylim=c(0,6),main=paste(cname,"\n",n1,n2,n12),xlab="Extreme of efficacy (l2fc)",ylab="|delta(log10(AC50))|")
      }
      x <- 0
      for(i in 1:length(bin.list)) {
        if(count.list[i]>0) {
          x <- x+1
          text(x,6,count.list[i])
        }
      }
      x1a <- x1[replicate.gt.cutoff==1]
      x2a <- x2[replicate.gt.cutoff==1]
      x1b <- x1[replicate.gt.cutoff== -3]
      x2b <- x2[replicate.gt.cutoff== -3]
      h1a <- h1[replicate.gt.cutoff==1]
      h2a <- h2[replicate.gt.cutoff==1]
      h1b <- h1[replicate.gt.cutoff== -3]
      h2b <- h2[replicate.gt.cutoff== -3]
      mask <- h1b*h2b
      x1b <- x1b[mask==1]
      x2b <- x2b[mask==1]
      
      mask <- h1*h2
      x1m <- x1[mask==1]
      x2m <- x2[mask==1]
      if(length(x1m)>0 || length(x1m)>0) {
        plot(x2m~x1m,type="p",pch=".",xlab=paste0(samples[1]," log10(AC50)"),
             ylab=paste0(samples[2]," log10(AC50)"),xlim=c(-3,3),
             ylim=c(-3,3),
             main=paste(cname,"\n",nboth.gt.cutoff,n3.gt.cutoff,n1.gt.cutoff,n2.gt.cutoff))
        lines(c(-6,6),c(-6,6))
        points(x2b~x1b,pch=21,bg="blue")
        points(x2a~x1a,pch=21,bg="red")
      }
      else {
        plot(c(1,1),type="n",xlab=paste0(samples[1]," log10(AC50)"),ylab=paste0(samples[2]," log10(AC50)"),xlim=c(-3,3),ylim=c(-3,3),main=paste(cname,"\n",nboth.gt.cutoff,n3.gt.cutoff,n1.gt.cutoff,n2.gt.cutoff))
        lines(c(-6,6),c(-6,6))
      }
      breaks <- c(seq(from=-4,to=4,by=0.25))
      x <- x1[h1==1]
      hist(x,breaks=breaks,main=paste(cname,"\n",samples[1]),xlab="log(AC50)")
      x <- x2[h2==1]
      hist(x,breaks=breaks,main=paste(cname,"\n",samples[2]),xlab="log(AC50)")
      x <- x1[h1==1]
      y <- y1[h1==1]
      plot(y1~x1,xlab="log10(AC50)",ylab="l2fc",xlim=c(-3,3),ylim=c(-5,5),main=paste(cname,"\n",samples[1]))
      x <- x1[h1==1]
      y <- y1[h1==1]
      points(y~x,pch=21,bg="red")
      lines(c(-5,5),c(0,0))
      lines(c(-5,5),c(cutoff,cutoff))
      lines(c(-5,5),c(-cutoff,-cutoff))
      plot(y2~x2,xlab="log10(AC50)",ylab="l2fc",xlim=c(-3,3),ylim=c(-5,5),main=paste(cname,"\n",samples[2]))
      x <- x2[h2==1]
      y <- y2[h2==1]
      points(y~x,pch=21,bg="red")
      lines(c(-5,5),c(0,0))
      lines(c(-5,5),c(cutoff,cutoff))
      lines(c(-5,5),c(-cutoff,-cutoff))
    }
    if(!to.file) browser()
  }
  if(to.file) graphics.off()
}