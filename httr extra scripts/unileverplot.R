library(gplots)
library(readxl)

unileverplot = function(method = "fc",metric = "top"){
  file = paste0("output/pathway_conc_resp_summary/PATHWAY_CR_biohall_analogs_",method,".RData")
  load(file)
  
  PATHWAY_CR$top_minus_cutoff = (PATHWAY_CR$top - sign(PATHWAY_CR$top)*PATHWAY_CR$cutoff)*PATHWAY_CR$hitcall
  PATHWAY_CR$logac50 = log10(PATHWAY_CR$ac50)
  PATHWAY_CR$logac50[is.na(PATHWAY_CR$logac50)] = 0
  PATHWAY_CR$top_times_ac50 = PATHWAY_CR$top*(3 - PATHWAY_CR$logac50)
  
  # PATHWAY_CR$max_response = sapply(strsplit(test,"\\|"), function(x){
  #     x = as.numeric(x)
  #     loc = which.max(abs(x))
  #     return(x[loc])
  #   })
  
  cnames = unique(PATHWAY_CR$name)
  hitlist =lapply(as.list(cnames), function(x){
      output = PATHWAY_CR[PATHWAY_CR$name == x,metric[1]]
      names(output) = PATHWAY_CR$pathway[PATHWAY_CR$name == x]
      output = output[order(names(output))]
      return(output)
    })
  
  hitmat = Reduce(cbind, hitlist)
  colnames(hitmat) = cnames
  
  analogs = unileveranalogs()
  
  colcolors =rep("black", ncol(hitmat))
  # othercols = rainbow(length(analogs))
  othercols = c("deepskyblue2", "darkgoldenrod", "darkred", "chartreuse3", "deeppink3",  "darkorchid3", "blue","darkgreen", 
                "red")
  for(i in 1:length(analogs)){
    colcolors[cnames %in% analogs[[i]] ] = othercols[i]
  }
  # colcolors[cnames %in% c("Caffeine", "Theophylline", "Theobromine")] = "brown"
  # colcolors[cnames %in% c("Curcumin", "Tetrahydrocurcumin", "Bisdemethoxycurcumin")] = "blue"
  # colcolors[cnames %in% c("Umbelliferone", "Coumarin" )] = "red"
  
  #NOTE: REMOVE BLOCK 5 chems
  
  eqrow = apply(hitmat,1, function(x){all(x == x[1])})
  
  hitmat = hitmat[!eqrow,]
  # qbreaks = quantile(hitmat[hitmat != 0], c(.01,.1,.2,.3,.4,.6,.7,.8,.9,.99))
  # qbreaks = sort(unique(c(qbreaks, -.01, .01)))
  newmin = min(abs(quantile(hitmat[hitmat != 0], c(.01,.99))))
  n = 20
  qbreaks = seq(-newmin,newmin,length.out=n)
  
  title = paste0("Unilever Analogs, Metric: \n",metric)
  if(metric == "top_times_ac50") title = paste0("Unilever Analogs, Metric: \nTop*(3-Log10(AC50))")
  
  # outfile = paste0("output/unilever_analogs/heatmap_",method, "_",metric, "_wards.pdf")
  # pdf(file = outfile, width = 7, height = 7)
  
  hclust.method = function(x){hclust(x, method = "ward.D2")}
  # heatmap(hitmat, labRow = NA, margins = c(13,0), ColSideColors = colcolors)
  heatmap.2(hitmat, labRow = NA, margins = c(13,.5), colCol = colcolors, trace = "none", col = "bluered", 
            breaks = qbreaks, main = title, hclustfun = hclust.method, cexCol = 0.275 + 1/log10(ncol(hitmat)))
  
  # dev.off()
}

unileveranalogs = function(){
  analogs = read_xls("unilever_analogs.xls")
  output = split(analogs$PREFERRED_NAME, analogs$groupname)
  output$ddt = c("p,p'-DDT", "p,p'-DDD", "p,p'-DDE", "Methoxychlor")
  load(file = "input/fcdata/CHEM_DICT_Phase1_6fixed.RData")
  allchems = unique(CHEM_DICT$name)
  output$conazole = allchems[grepl("conazole",allchems, ignore.case = T)]
  output$statin = allchems[grepl("statin",allchems, ignore.case = T)]
  output$phthalate = allchems[grepl("phthalate",allchems, ignore.case = T)]
  
  return(output)
}

allunileverplots = function(){
  for(method in c("fc", "gsva", "mygsea")){
    for(metric in c("top")){
      unileverplot(metric = metric, method = method)
    }
  }
}