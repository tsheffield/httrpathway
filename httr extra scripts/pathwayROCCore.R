library(openxlsx)
library(optimx)
library(stringr)
source("R/general_utils.R")
source("R/httrFit.R")
source("R/hill.R")
source("R/acx.R")
source("R/tcplHillLogc.R")
source("R/tcplGnlsLogc.R")
#--------------------------------------------------------------------------------------
#' Perform concentration-response analysis for all pathways in a pathway set
#' across all chemicals
#'
#' @param to.file If TRUE, plot the daa to a pdf file
#' @param pathset Name of the pathway set
#' @param sysdate The data the files were generated
#' @param bmad The BMAD value for that pathway set
#'
#' @return Nothing is returned
#' Two files are generated, a pdf of the plots and an Excel of the fit parameters
#'
#--------------------------------------------------------------------------------------
pathwayROCCore <- function(sample_id,
                                to.file=F,
                                pathset,
                                dataset,
                                method,
                                pathscoremat,
                                pathmat,
                                bmadmat,
                                pathway_annotations,
                                bmad_factors,
                                pathway) {
  rownames(bmadmat) <- bmadmat[,"pathway"]
  #sample_id <- "EP000004A02"
  tryCatch({
    
    name.list <- c("sample_id","dsstox_substance_id","casrn","name",
                   "time","pathset","pathway","pathway_class","pathway_size",
                   "dataset","method","n_gt_cutoff","cutoff",
                   "fit_method",
                   "top_over_cutoff",
                   "rmse",
                   "hill_tp","hill_ga","hill_gw",
                   "gnls_tp","gnls_ga","gnls_gw","gnls_la","gnls_lw",
                   "hitcall",
                   "ac50","ac50_loss","hill","top",
                   "ac5","ac10","ac20","ac3bmad","bmd10",
                   "conc",
                   "resp")
    
    row <- as.data.frame(matrix(nrow=1,ncol=length(name.list)))
    names(row) <- name.list
    pathscoremat.sid <- pathscoremat[is.element(pathscoremat[,"sample_id"],sample_id),]
    dsstox_substance_id <- pathscoremat.sid[1,"dsstox_substance_id"]
    time <- pathscoremat.sid[1,"time"]
    casrn <- pathscoremat.sid[1,"casrn"]
    chemical_name <- pathscoremat.sid[1,"name"]
    chemical_name_proper <- chemical_name
    chemical_name_proper <- str_replace_all(chemical_name_proper,"\\)","")
    chemical_name_proper <- str_replace_all(chemical_name_proper,"\\(","")
    chemical_name_proper <- str_replace_all(chemical_name_proper,":","")
    
    cat(chemical_name_proper,"\n")
    conc.list <- sort(unique(pathscoremat.sid[,"conc"]))
    path.list <- sort(unique(pathscoremat.sid[,"pathway"]))
    nconc <- length(conc.list)
    omat <- NULL

    hitcalls = rep(-1, length(bmad_factors))
    
    for(i in 1:length(bmad_factors)) {
      
      bmad_factor = bmad_factors[i]
      pathway_class <- pathway_annotations[is.element(pathway_annotations[,"pathway"],pathway),"pclass"]
      if(sum(nchar(pathway_class))==0) pathway_class <- "-"
      pset <- pathmat[is.element(pathmat[,"pathway"],pathway),"pathset"]
      pset <- pset[1]
      pathscoremat.pathway <- pathscoremat.sid[is.element(pathscoremat.sid[,"pathway"],pathway),]
      pathway_size <- pathscoremat.pathway[1,"size"]
      
      
      bmad <- bmadmat[pathway,"mad"]
      bmed <- bmadmat[pathway,"median"]
      
      hitcall = 0
      if(!is.na(bmad)) {
        cutoff <- bmad_factor*bmad
        if(method == "fisher") cutoff = -log(.1)*bmad_factor 

        vals <- pathscoremat.pathway[,"pathway_score"]-bmed
        vals <- vals[abs(vals)>cutoff]
        n_gt_cutoff <- length(vals)
        j <- 1
        tempj <- pathscoremat.pathway
        conc <- tempj[,"conc"]
        resp <- tempj[,"pathway_score"]-bmed
        mask <- resp
        mask[] <- 1
        mask[is.na(resp)] <- 0
        conc <- conc[mask==1]
        resp <- resp[mask==1]
        success <- F
        tryCatch({
          params <- httrFit(log10(conc), resp, bmad, bmad_factor = bmad_factor, force.fit = F, bidirectional = T)
          success <- T
        }, warning = function(w1) {
          print(w1)
          success <- T
        }, error = function(e1) {
          print(e1)
          browser()
          success <- F
        })

        if(success) {
          plot_params <- NULL
          ac50 <- NA
          hill <- NA
          top <- 0
          ac50_loss <- NA
          fit_method <- "cnst"
          rmse <- params$cnst_rmse
          dofit <- F
          if(!is.na(params$hill)) {
            if(params$hill==1 && params$hill_aic < params$gnls_aic && params$hill_aic < params$cnst_aic ) {
              ac50 <- 10**params$hill_ga
              top <- params$hill_tp
              hill <- params$hill_gw
              rmse <- params$hill_rmse
              fit_method <- "hill"
              plot_params <- c()
              dofit <- T
            }
          }
          if(!dofit) {
            if(!is.na(params$gnls)) {
              if(params$gnls==1 && params$gnls_aic < params$cnst_aic ) {
                ac50 <- 10**params$gnls_ga
                ac50_loss <- 10**params$gnls_la
                top <- params$gnls_tp
                hill <- params$gnls_gw
                rmse <- params$gnls_rmse
                fit_method <- "gnls"
                if((ac50/ac50_loss) < 50) {
                  ac50 <- 10**params$hill_ga
                  ac50_loss <- NA
                  top <- params$hill_tp
                  hill <- params$hill_gw
                  rmse <- params$hill_rmse
                  fit_method <- "hill"
                  if(!to.file) cat("flipped method gnls to hill\n")
                }
                dofit <- T
              }
            }
          }
           
          rbmad <- abs(resp)
          rbmad[rbmad<cutoff] <- 0
          rbmad[rbmad>0] <- 1
          cbmad <- conc[rbmad==1]
          hitcall <- 0
          if(n_gt_cutoff>0) hitcall <- 1
          if(abs(top)<cutoff) hitcall <- 0
          #browser()
          
          if(hitcall==1 && fit_method=="gnls") {
            l1 <- log10(ac50)
            l2 <- log10(ac50_loss)
            if(abs(l1-l2)<0.5) hitcall <- 0
            if(n_gt_cutoff==1) hitcall <- 0
            if(hitcall==1) {
              success <- 0
              state0 <- 0
              state1 <- 0
              for(ir in 1:length(resp)) {
                state0 <- state1
                state1 <- 0
                if(resp[ir]>cutoff) state1 <- 1
                if(resp[ir]< -cutoff) state1 <- -1
                if(state1==state0 && abs(state1)>0) success <- 1
              }
              if(success==0) hitcall <- 0
            }
          }
          if(hitcall==1) {
            if(!is.na(ac50)) {
              if(ac50<10 && n_gt_cutoff==1) hitcall <- 0
            }
            if(n_gt_cutoff==2) {
              x <- conc[abs(resp)>cutoff]
              y <- resp[abs(resp)>cutoff]
              if((max(x)/min(x)) > 30) hitcall <- 0
              if(max(x)<1) hitcall <- 0
            }
          }

        } 
        
      }
      
      hitcalls[i] = hitcall
    }	
  }, warning = function(w) {
    print(w)
    success <- T
  }, error = function(e) {
    print(e)
    browser()
    success <- F
  })
  return(hitcalls)
}