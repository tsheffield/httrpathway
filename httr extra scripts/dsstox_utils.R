#--------------------------------------------------------------------------------------
#
# dsstox_utils.R Utilities to get inforamtion from the dsstox database
#
# October 2017
# Richard Judson
#
# US EPA
# Questions, comments to: judson.richard@epa.gov
#
# the default assumtion is that the server is "mysql-res1.epa.gov" and
# the database instance is ro_stg_dsstox
#
#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
#' Get the casrn and name given a DSSTox CID
#'
#' @param gsid generic dsstox_substance_id
#' @param verbose if TRUE print input
#'
#' @return list containing the generic dsstox_substance_id, CASRN, preferred name
#'
#--------------------------------------------------------------------------------------
dsstox.sid.casrn.name <- function(gsid,verbose=F) {
  if(verbose) printCurrentFunction(gsid)
  query <- paste("select casrn,preferred_name from generic_substances where dsstox_substance_id='",gsid,"'",sep="")
  ret <- runQuery(query,"ro_stg_dsstox",T,F)
  casrn <- ret[1,1]
  cname <- ret[1,2]
  if(verbose) cat(casrn,"\n")
  return(list(dsstox_substance_id=gsid,casrn=casrn,name=cname))
}
#--------------------------------------------------------------------------------------
#' Get the casrn and name given a DSSTox gsid
#'
#' @param gsid Generic dsstox_substance_id
#' @param verbose If TRUE print input
#'
#' @return List containing the generic dsstox_substance_id, CASRN, preferred name
#'
#--------------------------------------------------------------------------------------
dsstox.gsid.to.casrn_name <- function(gsid,verbose=F) {
  if(verbose) printCurrentFunction(gsid)
  query <-  paste("select dsstox_substance_id,casrn,preferred_name from generic_substances where id='",gsid,"'",sep="")
  ret <- runQuery(query,"ro_stg_dsstox",T,F)
  dsstox_substance_id <- ret[1,1]
  casrn <- ret[1,2]
  cname <- ret[1,3]
  if(verbose) cat(dsstox_substance_id,":",casrn,":",cname,"\n")
  return(list(dsstox_substance_id=dsstox_substance_id,casrn=casrn,cname=cname))
}
#--------------------------------------------------------------------------------------
#' Get the casrn and name given a DSSTox CID
#'
#' @param dsstox.cid DSSTox compound id
#' @param verbose If TRUE print input
#'
#' @return List containing the generic dsstox_substance_id, CASRN, preferred name
#'
#--------------------------------------------------------------------------------------
dsstox.cid.casrn.name <- function(dsstox.cid,verbose=F) {
  printCurrentFunction(dsstox.cid)
  query <- paste("select id from compounds where dsstox_compound_id='",dsstox.cid,"'",sep="")
  cid <- runQuery(query,"ro_stg_dsstox",T,F)[1,1]
  query <- paste("select fk_generic_substance_id from generic_substance_compounds where fk_compound_id=",cid,sep="")
  gsid <- runQuery(query,"ro_stg_dsstox",T,F)[1,1]
  if(!is.na(gsid)) {
    query <- paste("select casrn,preferred_name,dsstox_substance_id from generic_substances where id=",gsid,sep="")
    ret <- runQuery(query,"ro_stg_dsstox",T,F)
    casrn <- ret[1,1]
    cname <- ret[1,2]
    dsstox.gsid <- ret[1,3]
  }
  else {
    casrn <- paste("NOCASRN_",dsstox.cid,sep="")
    cname <- paste("NONAME_",dsstox.cid,sep="")
    dsstox.gsid <- paste("NOGSID_",dsstox.cid,sep="")
  }
  if(verbose) cat(dsstox.cid,":",cid,":",gsid,":",casrn,":",cname,":",dsstox.gsid,"\n")
  return(list(dsstox_substance_id=dsstox.gsid,casrn=casrn,name=cname))
}
#--------------------------------------------------------------------------------------
#' Get the casrn and name given a DSSTox source substance id
#'
#' @param dsstox_source_substance_id dsstox_source_substance_id
#' @param verbose If TRUE print input
#'
#' @return List containing the generic dsstox_substance_id, CASRN, preferred name
#'
#--------------------------------------------------------------------------------------
dsstox.source_substance_id.to.casrn_name <- function(dsstox_source_substance_id,verbose=F) {
  if(verbose) printCurrentFunction(dsstox_source_substance_id)
  query <-  paste("select fk_generic_substance_id from source_generic_substance_mappings where fk_source_substance_id=",dsstox_source_substance_id,sep="")
  ret <- runQuery(query,"ro_stg_dsstox",T,F)
  fk_generic_substance_id <- ret[1,1]
  if(verbose) cat("fk_generic_substance_id:",fk_generic_substance_id,"\n")
  query <-  paste("select dsstox_substance_id,casrn,preferred_name from generic_substances where id='",fk_generic_substance_id,"'",sep="")
  ret <- runQuery(query,"ro_stg_dsstox",T,F)
  dsstox_substance_id <- ret[1,1]
  casrn <- ret[1,2]
  cname <- ret[1,3]
  if(verbose) cat(dsstox_substance_id,":",casrn,":",cname,"\n")
  return(list(dsstox_substance_id=dsstox_substance_id,casrn=casrn,cname=cname))
}

#--------------------------------------------------------------------------------------
#' Get the everything given a casrn
#'
#' @param casrn CASRN
#' @param verbose If TRUE print input
#'
#' @return List containing casrn, preferred name, dsstox_substance_id, dsstox_compound_id, smile
#'
#--------------------------------------------------------------------------------------
dsstox.casrn.to.all <- function(casrn,verbose=F) {
  if(verbose) printCurrentFunction(casrn)
  cname <- "-"
  dsstox_substance_id <- "-"
  dsstox_compound_id <- "-"
  smiles <- ""
  query1 <- paste("select count(*) from generic_substances where casrn='",casrn,"'",sep="")
  count <- runQuery(query1,"ro_stg_dsstox",T,F)[1,1]
  if(count==1) {
    query1 <- paste("select id,dsstox_substance_id,preferred_name from generic_substances where casrn='",casrn,"'",sep="")
    res1 <- runQuery(query1,"ro_stg_dsstox",T,F)
    id <- res1[1,1]
    dsstox_substance_id <- res1[1,2]
    cname <- res1[1,3]

    query3 <- paste("select count(*) from generic_substance_compounds where fk_generic_substance_id=",id,sep="")
    count <- runQuery(query3,"ro_stg_dsstox",T,F)[1,1]
    if(count>0) {
      query3 <- paste("select fk_compound_id from generic_substance_compounds where fk_generic_substance_id=",id,sep="")
      cid <- runQuery(query3,"ro_stg_dsstox",T,F)[1,1]
      query3 <- paste("select dsstox_compound_id,smiles from compounds where id=",cid,sep="")
      dsstox_compound_id <- runQuery(query3,"ro_stg_dsstox",T,F)[1,1]
      smiles <- runQuery(query3,"ro_stg_dsstox",T,F)[1,2]
    }
  }
  return(list(casrn=casrn,cname=cname,dsstox_substance_id=dsstox_substance_id,dsstox_compound_id=dsstox_compound_id,smiles=smiles))
}
#--------------------------------------------------------------------------------------
#' Get the chemical casrn and name given an inchi or SMILES
#'
#' @param inchi inchi
#' @param smiles SMILES
#' @param verbose If TRUE print input
#'
#' @return List including casrn and name (cname)
#'
#--------------------------------------------------------------------------------------
dsstox.inchi.smiles.to.all <- function(inchi,smiles,verbose=F) {
  if(verbose) printCurrentFunction()
  casrn <- "-"
  cname <- "-"
  dsstox_substance_id <- "-"
  dsstox_compound_id <- "-"
  query <- paste("select count(*) from compounds where inchi='",inchi,"'",sep="")
  count <- runQuery(query,"ro_stg_dsstox",T,F)[1,1]
  if(verbose) cat("count from InCHI: ",count,"\n")
  browser()
  if(count==1) {
    query <- paste("select id from compounds where inchi='",inchi,"'",sep="")
    cid <- runQuery(query,"ro_stg_dsstox",T,F)[1,1]
    res <- dsstox.cid.casrn.name(cid,verbose)
    if(verbose) cat("Found from InCHI: ",res$cname,"\n")
    return(list(casrn=res$casrn,cname=res$cname))
  }
  else {
    query <- paste("select count(*) from compounds where smiles='",smiles,"'",sep="")
    count <- runQuery(query,"ro_stg_dsstox",T,F)[1,1]
    if(verbose) cat("count from SMILES: ",count,"\n")
    if(count==1) {
      query <- paste("select id from compounds where smiles='",smiles,"'",sep="")
      cid <- runQuery(query,"ro_stg_dsstox",T,F)[1,1]
      res <- dsstox.cid.casrn.name(cid,verbose)
      if(verbose) cat("Found from SMILES: ",res$cname,"\n")
      return(list(casrn=res$casrn,cname=res$cname))
    }
    else {
      if(verbose) cat("Not found\n")
      return(list(casrn="NOCASRN",cname="NONAME"))
    }
  }
}
#--------------------------------------------------------------------------------------
#' Create and R data frame of the DSSTox generic_substances table
#'
#' @param nrow The number of rows to get, -1 indicaes get all rows
#' @param verbose If TRUE print input
#'
#' @return Data frame with the table
#'
#--------------------------------------------------------------------------------------
dsstox.generic_substances_table <- function(nrow=-1,verbose=F) {
  if(verbose) printCurrentFunction()
  if(nrow>0) mat <- runQuery(paste("select dsstox_substance_id, casrn,preferred_name,qc_notes,substance_type,id as gsid from generic_substances limit ",nrow,sep=""),"ro_stg_dsstox")
  else mat <- runQuery("select dsstox_substance_id, casrn,preferred_name,qc_notes,substance_type,id as gsid from generic_substances","ro_stg_dsstox")
  return(mat)
}
