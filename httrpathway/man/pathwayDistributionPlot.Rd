% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/pathwayDistributionPlot.R
\name{pathwayDistributionPlot}
\alias{pathwayDistributionPlot}
\title{Pathway Distribution Plot}
\usage{
pathwayDistributionPlot(pathset = "bhrr",
  dataset = "ph1_100normal_gene", method = "fc",
  nullset = "ph1_100normal_gene_RAND125", perc = 0.95, fdr = 0.25,
  comparetype = "Null",
  samplepaths = c("HALLMARK_ESTROGEN_RESPONSE_EARLY",
  "DUTERTRE_ESTRADIOL_RESPONSE_6HR_UP", "HALLMARK_CHOLESTEROL_HOMEOSTASIS",
  "Vitamin A and carotenoid metabolism", "Cytochrome P450 pathway",
  "HALLMARK_ANDROGEN_RESPONSE"), to.file = T, seed = 12345)
}
\arguments{
\item{pathset}{Name of pathway set.}

\item{dataset}{Name of data set.}

\item{method}{Name of pathway scoring method.}

\item{nullset}{Name of null data set.}

\item{perc}{1-p-value for pvalue cutoff.}

\item{fdr}{False discovery rate for FDR cutoff.}

\item{comparetype}{Type of noise to use: "Null" for null data scores, 
"Low Conc" for lowest concentrations.}

\item{samplepaths}{Vector of sample pathway names to plot.}

\item{to.file}{If to.file = T, write plot to disk.}

\item{seed}{Randomization seed to use to choose additional sample pathways.}
}
\value{
No output.
}
\description{
Plots null and actual pdfs for given pathway and cutoffs.
}
\details{
This function requires that a PATHSCOREMAT file has already been generated
for the given pathset/dataset/method using pathwayScore. There should also
be PATHSCOREMAT file for the nullset if comparetype = "Null". This function
has also been used to get crossing-based cutoffs, but that feature has been
deprecated.
}
